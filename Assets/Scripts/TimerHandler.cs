﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class TimerHandler : MonoBehaviour {
	public Text timerText;

	private GameController gc;
	private int[] time;
	private bool isFlashing;
	private bool isRunning;

	void Start () {
		gc = (GameController)Camera.main.GetComponent ("GameController");
	}

	public void StartCount(){
		isFlashing = false;
		timerText.color = Color.white;
		isRunning = true;
		StartCoroutine (AnimateClock ());
	}

	public void FlashTimer(float freq){
		timerText.color = Color.red;
		StartCoroutine (FlashAnimation (freq));
	}

	public void ShowText(string category, Color color){
		isRunning = false;
		isFlashing = false;
		timerText.color = color;
		timerText.text = category;
	}

	public void PrepareTimer(int t) {//t in miliseconds
		time = MilisecondsToTime (t);
		timerText.text = FormatTime (time);
	}

	public void Clear() {
		timerText.text = "";
		isFlashing = false;
		isRunning = false;
	}

	public void Pause() {
		isRunning = false;
		Debug.Log ("Pausing timer");
	}

	public void Show(){
		gameObject.SetActive (true);
	}

	public void Hide(){
		gameObject.SetActive (false);
	}

	int[] MilisecondsToTime(int t) {
		int seconds = (int)TimeSpan.FromMilliseconds (t).TotalSeconds;
		int minutes = (int)TimeSpan.FromMilliseconds (t).TotalMinutes;
		int centiseconds = (int)((t - TimeSpan.FromSeconds(seconds).TotalMilliseconds - TimeSpan.FromMinutes(minutes).TotalMilliseconds)/10.0f);
		return new int[] {minutes, seconds, centiseconds};
	}
	
	string FormatTime(int[] t) {
		return t [0].ToString ("00") + ":" + t [1].ToString ("00");// + ":" + t [2].ToString ("00");
	}

	IEnumerator AnimateClock() {
		int[] auxTime = time;
		while (isRunning) {
			auxTime[2] -= 1;
			if(auxTime[2] == -1){
				auxTime[2] = 99;
				auxTime[1] -=1;
				if(auxTime[1] == -1) {
					auxTime[1] = 0;
					auxTime[0] -= 1;
				}
			}
			if(auxTime[2] == 0 && auxTime[1] == 0 && auxTime[0] == 0) {
				break;
			}
			timerText.text = FormatTime (auxTime);
			yield return new WaitForSeconds(0.01f);
		}
		time = auxTime;
		gc.FinishedCountdown ();
	}

	IEnumerator FlashAnimation(float freq) {
		isFlashing = true;
		string value = timerText.text;
		while (isFlashing) {
			timerText.text = "";
			yield return new WaitForSeconds(freq/2.0f);
			timerText.text = value;
			yield return new WaitForSeconds(freq/2.0f);
		}

	}
}
