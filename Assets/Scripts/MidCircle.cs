﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MidCircle : MonoBehaviour {
	bool isIdle;
	bool isRotating;
	float speed = 2.0f;
	float oldSpeed;

	public void Launch(){
		StartCoroutine (StartAnimating ());
	}

	public void Hide(){
		RemoveImpulse ();
		GetComponent<Image> ().fillAmount = 0;
	}

	public void StartRotating(){
		isRotating = true;
		speed *= 10;
		AddImpulse ();
	}

	public void RemoveImpulse(){
		isRotating = false;
		isIdle = true;
		speed = 2.0f;
	}

	public void SlowDown(){
		speed = 2.0f;
	}

	public void AddImpulse(){
		oldSpeed = speed;
		speed *= 5;
		StartCoroutine (Decelerate ());
	}
	
	IEnumerator StartAnimating(){
		animation.Play ("circle_mid_start");
		yield return new WaitForSeconds(animation.GetClip("circle_mid_start").length);
		isIdle = true;
		float newAngle = Random.Range(400,320);
		while (isIdle) {
			float currentAngle = transform.rotation.eulerAngles.z < 320 ? transform.rotation.eulerAngles.z + 360 : transform.rotation.eulerAngles.z;
			if(!isRotating){
				if(Mathf.Abs(currentAngle - newAngle) < 10.0f){
					float tempAngle = Random.Range(400,320);
					if(Mathf.Abs(tempAngle - newAngle) > 10){
						newAngle = tempAngle;
					}
					
				}
			}
			else {
				newAngle = transform.rotation.eulerAngles.z - speed;
				if(newAngle + speed >= 360){
					newAngle -= 360;
				}
			}

			//Debug.Log(transform.rotation.eulerAngles + " | " + newAngle);
			float spd = speed * Time.deltaTime;
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis(newAngle, Vector3.forward), spd);
			yield return new WaitForSeconds(Time.deltaTime);
		}
	}

	IEnumerator Decelerate(){
		yield return new WaitForSeconds(0.5f);
		while (speed > oldSpeed) {
			speed -= speed/30;
			if(speed < oldSpeed){
				speed = oldSpeed;
			}
			yield return new WaitForSeconds(Time.deltaTime);
		}
	}
}
