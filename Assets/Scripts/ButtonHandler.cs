﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ButtonHandler : MonoBehaviour {
	public int bpm;
	public float lastTouchTime;
	public float touchDeltaTime;
	public int bpmAvg;
	public int touches;

	public Transform circle_tap;
	public Transform text;
	public Transform circle_inner;
	public Transform circle_mid;
	public Transform circle_pre_inner;
	public Transform circle_progress;
	public Transform shine;
	public Transform circle_post_middle;
	public Transform circle_out;
	public Transform container_bpm;
	public Transform container_avg_bpm;
	public BPMCircle bpmCircle;
	public BPMCircle bpmAvgCircle;

	List<float> savedBpm;
	bool isRunning;
	float timeStarted;
	float nextUpdateCheck;
	bool isFirstTouch;
	bool isWaitingForFirstLevelTouch;
	bool isChangingLevel;
	bool isBpmShown;
	float animationForwardSpeed = 1;
	float animationBackwardSpeed = -2f;
	float timeThresold = 0.1f; //the minimum value between touches to calculate bpm
	float increasedLevelTime;
	float timeBeforeDecreasingLevel = 1.0f;

	GameController gc;
	ProgressCircle progress;

	void Start(){
		gc = Camera.main.GetComponent<GameController> ();
		progress = circle_progress.GetComponent<ProgressCircle> ();
		savedBpm = new List<float> ();
		Hide ();
	}

	void Update() {
		if (isRunning) {
			if(bpm < 0){
				bpm = 0;
			}
			if(nextUpdateCheck < Time.time){
				nextUpdateCheck = Time.time + 0.25f;
				//RefreshBpm();
				//Check progress
				if(!isChangingLevel && !isWaitingForFirstLevelTouch){
					if(progress.GetProgress() <= 0 && gc.GetLevel(this) > 0){
						ProgressEmptied();
					}
					if(progress.GetProgress() > 0.99f && gc.GetLevel(this) == 3){
						progress.SetProgress(0.99f);
					}
				}
				else if(!isChangingLevel && isWaitingForFirstLevelTouch){//just after increasing level
					if(Time.time - increasedLevelTime > timeBeforeDecreasingLevel && gc.GetLevel(this) > 0){
						ProgressEmptied();
					}
				}
			}

		}
	}

	public void Show(){
		StartCoroutine(Launch ());
	}

	public void Hide(){
		HideImage (circle_tap.GetComponent<Image> ());
		text.GetComponent<Image> ().color = Color.clear;
		HideImage (circle_inner.GetComponent<Image> ());
		circle_mid.GetComponent<MidCircle> ().Hide ();
		HideImage (circle_pre_inner.GetComponent<Image> ());
		progress.Hide();
		HideImage (circle_post_middle.GetComponent<Image> ());
		circle_out.GetComponent<OutCircle>().Hide();
		circle_tap.animation.Stop ();
		container_bpm.transform.localScale = Vector3.zero;
		container_avg_bpm.transform.localScale = Vector3.zero;
		text.animation.Stop ();
		savedBpm = new List<float> ();
		bpmCircle.SetBPM (0);
		bpm = 0;
		bpmAvgCircle.SetBPM (0);
		bpmAvg = 0;
		savedBpm = new List<float> ();
		isFirstTouch = true;
		isWaitingForFirstLevelTouch = true;
		bpmCircle.Reset ();
	}

	IEnumerator Launch(){
		yield return new WaitForSeconds (1.5f);
		circle_tap.animation.Play ("circle_tap_start");
		circle_pre_inner.animation.Play ("circle_pre_mid_start");
		circle_inner.GetComponent<InnerCircle> ().Launch();
		circle_mid.GetComponent<MidCircle> ().Launch ();
		yield return new WaitForSeconds(circle_tap.animation.GetClip("circle_tap_start").length);
		text.animation.Play ("text_glow");
		circle_tap.animation.CrossFade ("circle_tap_stay");
		container_avg_bpm.animation.Play ();
		gc.ReadyToStart ();
		isRunning = true;
		timeStarted = Time.time;
	}

	void RefreshBpm(){
		if(lastTouchTime == 0.0f){// first touch
			bpm = 0;
		}
		else {
			float deltaTime = Time.time - lastTouchTime;
			if(deltaTime < timeThresold){
				return;
			}
			else {
				bpm = (int)(60.0f/(deltaTime));
			}
		}

		bpmCircle.SetBPM (bpm);
		CalculateAvgBpm ();
	}

	void CalculateAvgBpm(){
		savedBpm.Add (bpm);
		float total = 0;
		foreach(float b in savedBpm){
			total += b;
		}
		bpmAvg = (int)(total/savedBpm.Count);
		bpmAvgCircle.SetBPM (bpmAvg);
	}

	public void AddProgress(){
		if (!gc.IsButtonActive (this)) {
			gc.SetActiveButton(this);
//			if(!isBpmShown){
//				ShowBPMCounter();
//			}
			return;
		}
		if (isChangingLevel) {
			return;
		}
		if (isFirstTouch) {
			ShowBPMCounter();
			isFirstTouch = false;
		}
		if (isWaitingForFirstLevelTouch) {
			isWaitingForFirstLevelTouch = false;
		}
		progress.increaseStep();
		if(progress.GetProgress() >= 1){
			progress.SetProgress(1);
			ProgressFilled();
		}
		RefreshBpm ();
		touchDeltaTime = Time.time - lastTouchTime;
		lastTouchTime = Time.time;
		touches++;
	}

	public void ProgressFilled(){
		int level = gc.GetLevel (this);
		increasedLevelTime = Time.time;
		if (level < 3) {
			isWaitingForFirstLevelTouch = true;
			int previousLevel = level;
			gc.NextLevel (this);
			StartCoroutine (AnimateLevel (previousLevel, gc.GetLevel(this)));
		}
	}

	public void ProgressEmptied(){
		isWaitingForFirstLevelTouch = false;
		int level = gc.GetLevel (this);
		if (level > 0) {
			int previousLevel = level;
			gc.PreviousLevel (this);
			StartCoroutine (AnimateLevel (previousLevel, gc.GetLevel(this)));
		}
	}

	public void ShowBPMCounter(){
		if (!isBpmShown) {
			isBpmShown = true;
			PlayAnimation (container_bpm, "bpm_start", 1);
		}
	}

	public void HideBPMCounter(){
		if(isBpmShown){
			isBpmShown = false;
			PlayAnimation (container_bpm, "bpm_start", -1);
		}
	}

	public IEnumerator AnimateLevel(int oldLevel, int newLevel){
		//Debug.Log (oldLevel + " to " + newLevel);
		isChangingLevel = true;
		if (oldLevel == 0 && newLevel == 1) {// 0 to 1
			circle_mid.GetComponent<MidCircle>().StartRotating();
			PlayAnimation(shine, "shine", animationForwardSpeed);
			PlayAnimation(circle_post_middle, "circle_post_mid_start", animationForwardSpeed);
			yield return new WaitForSeconds(0.5f);
			progress.SetProgress(0);
		}

		if (oldLevel == 1) {
			if(newLevel == 0){// 1 to 0
				circle_mid.GetComponent<MidCircle>().SlowDown();
				PlayAnimation(circle_post_middle, "circle_post_mid_start", animationBackwardSpeed);
				yield return new WaitForSeconds(0.5f);
				progress.SetProgress(0.99f);
			}
			if(newLevel == 2) {// 1 to 2
				circle_mid.GetComponent<MidCircle>().AddImpulse();
				PlayAnimation(shine, "shine", 1);
				PlayAnimation(circle_out, "circle_out_start", animationForwardSpeed);
				circle_out.GetComponent<OutCircle>().Launch();
				yield return new WaitForSeconds(0.5f);
				progress.SetProgress(0);
			}
		}

		if (oldLevel == 2) {
			if(newLevel == 1){// 2 to 1
				PlayAnimation(circle_out, "circle_out_start", animationBackwardSpeed);
				circle_out.GetComponent<OutCircle>().Stop ();
				yield return new WaitForSeconds(0.5f);
				progress.SetProgress(0.99f);
			}
			if(newLevel == 3){// 2 to 3
				shine.animation.Play("shine_big");
				yield return new WaitForSeconds(0.5f);
				progress.SetProgress(0.99f);
			}
		}
		progress.SetLevel (gc.GetLevel(this));
		isChangingLevel = false;

	}

	void PlayAnimation(Transform t, string anim, float speed){
		t.animation.Stop (anim);
		t.animation [anim].speed = speed;
		if (speed < 0) {
			t.animation[anim].time = t.animation[anim].length;
		}
		t.animation.Play (anim);
	}

	void HideImage (Image img){
		img.rectTransform.sizeDelta = Vector2.zero;
	}
}
