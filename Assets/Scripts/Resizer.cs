﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Resizer : MonoBehaviour {

	void Start () {
		if (isSameAspect (Camera.main.aspect, 16.0f / 9.0f)) {
			Debug.Log(this + "16:9");
		}
		else if (isSameAspect (Camera.main.aspect, 16.0f / 10.0f)) {
			ResizeTransform(0.9f,25);
			Debug.Log("16:10");
		}
		else
		{
			ResizeTransform(0.8f,50);
			Debug.Log(this + "4:3");
		}
	}

	void ResizeTransform(float scaleMultiplier, float displacement) {
		transform.localScale = new Vector3(transform.localScale.x*scaleMultiplier, transform.localScale.y*scaleMultiplier, transform.localScale.z);
		float newPos;
		if(transform.localPosition.x < 0){
			newPos = transform.localPosition.x - displacement;
		}
		else {
			newPos = transform.localPosition.x + displacement;
		}
		transform.localPosition = new Vector3(newPos, transform.localPosition.y, transform.localPosition.z);
	}
	
	bool isSameAspect(float a1, float a2){
		return Mathf.Abs (a1 - a2) <= 0.1f;
	}
}
