﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 
using System;


public class DoOnMainThread : MonoBehaviour 
{
	
	public static readonly Queue<Action> ExecuteOnMainThread = new Queue<Action>();
	
	public virtual void Update()
	{
		// dispatch stuff on main thread
		while (ExecuteOnMainThread.Count > 0)
		{
			//Debug.Log("Executing");
			ExecuteOnMainThread.Dequeue().Invoke();
		}
	}
}
