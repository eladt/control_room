﻿using UnityEngine;
using System.Collections;

public class InnerCircle : MonoBehaviour {
	bool animating;

	public void Launch(){
		StartCoroutine (StartAnimating ());
	}

	IEnumerator StartAnimating(){
		animation.Play ("circle_inner_start");
		yield return new WaitForSeconds(animation.GetClip("circle_inner_start").length);
		animating = true;
		float newAngle = 0.0f;
		while (animating) {
			if(Mathf.Abs(transform.rotation.eulerAngles.z - newAngle) < 20.0f){
				newAngle = Random.Range(0,360);
			}
			float speed = 2.0f * Time.deltaTime;
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis(newAngle, Vector3.forward), speed);
			yield return new WaitForSeconds(Time.deltaTime);
		}
	}
}
