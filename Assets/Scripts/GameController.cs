﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour, ISocketDelegate {
	public GameObject movieSplash;
	public GameObject movieQR1;
	public GameObject movieQR2;
	public GameObject movieReady;
	public GameObject movieGame1;
	public GameObject movieGame2;
	public ButtonHandler blueButton;
	public ButtonHandler redButton;
	public QRHandler qr;
	public GameObject movieTexture;
	public float buttonCooldown = 0.5f;
	public TimerHandler timer;
	public GameObject endGamePanel;
	public Text endGamePanelText;
	public bool isGameEnded;
	public GameObject btnRestart;
	public GameObject btnBack;

	SocketManager sm;
	ButtonHandler activeButton;
	bool isInCoolDown;
	int blueLevel;
	int redLevel;
	DoOnMainThread mainThread;
	bool isOffline;
	GameObject playingMovie;

	public enum ScreenTypes{
		ScreenTransition,
		ScreenQR,
		ScreenGame,
		ScreenGetReady
	}

	public ScreenTypes screen;
	bool isGameStarted;

	void Start(){
		sm = GetComponent<SocketManager> ();
		isGameEnded = true;
		//mainThread = new DoOnMainThread ();
		StartApp ();
	}

	void StartApp(){
		//movieTexture.SetActive (true);
		StartCoroutine (ShowQRScreen (false));
	}

	IEnumerator ShowQRScreen(bool splash){
		screen = ScreenTypes.ScreenTransition;
		if (splash) {
			PlayVideo(movieSplash);
			yield return new WaitForSeconds (5);
		}
		else {
			yield return new WaitForSeconds (0.5f);
		}
		qr.InitializeDecoder ();
		PlayVideo (movieQR1);
		yield return new WaitForSeconds (4);
		PlayVideo(movieQR2);
		screen = ScreenTypes.ScreenQR;
		qr.Activate ();
	}

	public void PauseQRScreen(){
		movieQR2.GetComponent<MMT.MobileMovieTexture>().Stop ();
	}
	public void ResumeQRScreen() {
		movieQR2.GetComponent<MMT.MobileMovieTexture>().Play ();
	}

	IEnumerator ShowReadyScreen(){
		screen = ScreenTypes.ScreenTransition;
		qr.Deactivate ();
		PlayVideo(movieReady);
		yield return new WaitForSeconds (0);
		//StartCoroutine(ShowGameScreen());
	}

	IEnumerator ShowGameScreen(){
		if(isOffline){
			btnRestart.SetActive(true);
			btnBack.SetActive (true);
		}
		screen = ScreenTypes.ScreenGame;
		PlayVideo (movieGame1);
		yield return new WaitForSeconds (6);
		PlayVideo (movieGame2);
		//movieTexture.SetActive (false);
		isGameEnded = false;
		yield return new WaitForSeconds (0);
	}

	IEnumerator ShowButtons(){
		blueButton.Show ();
		yield return new WaitForSeconds (0.5f);
		redButton.Show ();
	}

	IEnumerator Restart(float duration){
		blueButton.Hide ();
		redButton.Hide ();
		blueLevel = 0;
		redLevel = 0;
		StartCoroutine (ShowButtons ());
		yield return new WaitForSeconds (2.0f);
		isGameEnded = false;
		//timer.PrepareTimer((int)duration*1000);
		//timer.StartCount();
	}

	public void BackToQRScreen(){
		blueButton.Hide ();
		redButton.Hide ();
		blueLevel = 0;
		redLevel = 0;
		if(isOffline){
			btnRestart.SetActive(false);
			btnBack.SetActive (false);
		}
		StartCoroutine (ShowQRScreen (false));
	}

	void ChangeLevelForButton(int change, ButtonHandler btn){
		if (btn == blueButton && IsButtonAbleToChange(change, btn)) {
			if(!isOffline && change == 1){
				sm.sendVote(1);
			}
			blueLevel += change;
			Debug.Log ("Blue Level: " + blueLevel);
		}
		if (btn == redButton && IsButtonAbleToChange(change, btn)) {
			if(!isOffline && change == 1){
				sm.sendVote(0);
			}
			redLevel += change;
			Debug.Log ("Red Level: " + redLevel);
		}
	}

	bool IsButtonAbleToChange(int change, ButtonHandler btn){
		bool result = false;
		int level = 0;
		if (btn == blueButton) {
			level = blueLevel;
		}
		if (btn == redButton) {
			level = redLevel;
		}
		result = level + change < 3 || level + change >= 0;
		return result;
	}

	public void BlueTouch(){
		if (isGameStarted) {
			blueButton.AddProgress ();
		}
	}

	public void RedTouch(){
		if (isGameStarted) {
			redButton.AddProgress ();
		}
	}

	public void NextLevel(ButtonHandler btn){
		ChangeLevelForButton (1, btn);
	}

	public void PreviousLevel(ButtonHandler btn){
		ChangeLevelForButton (-1, btn);
	}

	public int GetLevel(ButtonHandler btn){
		int level = 0;
		if (btn == blueButton) {
			level = blueLevel;
		}
		if (btn == redButton) {
			level = redLevel;
		}
		return level;
	}
	
	public void ReadyToStart(){
		isGameStarted = true;
	}

	public void LoginToServer(string url){
		sm.Connect (this, url);
	}

	public void StartOfflineMode() {
		isOffline = true;
		StartCoroutine (ShowReadyScreen ());
		StartCoroutine(ShowGameScreen());
		 StartCoroutine(Restart(0));
	}

	public void SetActiveButton(ButtonHandler btn){
		if (!isInCoolDown) {
			if(activeButton){
				StartCoroutine(ChangeBPMCounters(activeButton, btn));
			}
			else {
				btn.ShowBPMCounter();
			}
			isInCoolDown = true;
			StartCoroutine(ButtonCoolDown(btn));
		}
	}

	IEnumerator ButtonCoolDown(ButtonHandler btn){
		yield return new WaitForSeconds (buttonCooldown);
		activeButton = btn;
		isInCoolDown = false;
	}

	IEnumerator ChangeBPMCounters(ButtonHandler oldBtn, ButtonHandler newBtn) {
		oldBtn.HideBPMCounter ();
		yield return new WaitForSeconds (0.75f);
		newBtn.ShowBPMCounter ();
	}

	public bool IsButtonActive(ButtonHandler btn){
		return activeButton == btn;
	}

	public void socketConnected(){
		Debug.Log ("CONNECTED");
		DoOnMainThread.ExecuteOnMainThread.Enqueue(() => { StartCoroutine (ShowReadyScreen ());});

	}

	public void socketError(){
		if (screen == ScreenTypes.ScreenGame) {
			//DoOnMainThread.ExecuteOnMainThread.Enqueue(() => { StartApp();});
			DoOnMainThread.ExecuteOnMainThread.Enqueue(() => {Application.LoadLevel(Application.loadedLevel);});
		}
		//sm.Connect (this);
		Debug.Log ("Connection Error");
	}

	public void socketClosed(){
		Debug.Log ("Connection Closed");
	}

	public void socketVoteOpened(int duration){
		Debug.Log ("Duration: " + duration);
		if(screen != ScreenTypes.ScreenGame){
			DoOnMainThread.ExecuteOnMainThread.Enqueue(() => {StartCoroutine(ShowGameScreen());});
			DoOnMainThread.ExecuteOnMainThread.Enqueue(() => { StartCoroutine(Restart(duration));}); 
			return;
		}
		DoOnMainThread.ExecuteOnMainThread.Enqueue(() => {
			PlayAnimation (endGamePanel.transform, "endGamePanel", -1);
		});
		DoOnMainThread.ExecuteOnMainThread.Enqueue(() => { StartCoroutine(Restart(duration));}); 
	}

	public void socketVoteClosed(){
		DoOnMainThread.ExecuteOnMainThread.Enqueue (() => {
						PlayAnimation (endGamePanel.transform, "endGamePanel", 1);});
		isGameEnded = true;
	}

	public void socketGameFinished() {
//		endGamePanelText.text = "GAME OVER";
//		isGameEnded = true;
//		DoOnMainThread.ExecuteOnMainThread.Enqueue (() => {
//			PlayAnimation (endGamePanel.transform, "endGamePanel", 1);});
		DoOnMainThread.ExecuteOnMainThread.Enqueue (() => {BackToQRScreen();});

	}

	public void FinishedCountdown (){
//		PlayAnimation (endGamePanel.transform, "endGamePanel", 1);
//		isGameEnded = true;
	}

	void PlayAnimation(Transform t, string anim, float speed){
		t.animation.Stop (anim);
		t.animation [anim].speed = speed;
		if (speed < 0) {
			t.animation[anim].time = t.animation[anim].length;
		}
		t.animation.Play (anim);
	}

	void PlayVideo(GameObject video){
		GameObject newMovie = (GameObject)Instantiate (video, transform.position, transform.rotation);
		if(playingMovie != null){
			Destroy(playingMovie);
		}
		playingMovie = newMovie;
	}
}
