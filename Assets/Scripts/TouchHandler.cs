﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class TouchHandler : MonoBehaviour {
	public GameObject touchEffect;
	public Transform blueButton;
	public Transform redButton;

	Vector3 blueButtonFlatPos;
	Vector3 redButtonFlatPos;
	GameController gc;

	void Start() {
		gc = GetComponent<GameController> ();
		blueButtonFlatPos = new Vector3(blueButton.position.x, blueButton.position.y,0);
		redButtonFlatPos = new Vector3(redButton.position.x, redButton.position.y,0);
	}

	void Update (){
		if (Input.GetButtonDown ("Fire1")) {
			Vector3 clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Pressed(new Vector3(clickPos.x, clickPos.y, 0));
		}
		int fingerCount = 0;
		foreach (Touch touch in Input.touches)
		{

			if (Input.touchCount > 0)
				print ("User has " + fingerCount + " finger(s) touching the screen");
			int pointerID = touch.fingerId;
			if (EventSystem.current.IsPointerOverGameObject(pointerID))
			{
				Pressed (touch.position);
				return;
			}
			
			if (touch.phase == TouchPhase.Ended)
			{
				// here we don't know if the touch was over an canvas UI
				return;
			}
		}
	}

	void Pressed(Vector3 position){
		//Debug.Log(position);
		if(gc.isGameEnded){
			return;
		}
		Vector3 flatPos = new Vector3 (position.x, position.y, 0);
		if (Vector3.Distance (flatPos, blueButtonFlatPos) < 1) {
			gc.BlueTouch();
			ShowTouch(blueButtonFlatPos);
		}
		if (Vector3.Distance (flatPos, redButtonFlatPos) < 1) {
			gc.RedTouch();
			ShowTouch(redButtonFlatPos);
		}

	}

	void ShowTouch(Vector3 position){
		GameObject go;
		go = Instantiate(touchEffect, position, Quaternion.identity) as GameObject;
		go.animation.Play ("btn_fadein");
		Destroy (go, 1.5f);
	}


}
