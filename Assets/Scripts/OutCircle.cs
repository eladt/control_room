﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OutCircle : MonoBehaviour {
	bool isIdle;
	bool isRotating;
	float speed = 2.0f;
	float oldSpeed;
	
	public void Launch(){
		StartCoroutine (StartAnimating ());
		isRotating = true;
		speed *= 20;
		AddImpulse ();
	}

	public void Hide(){
		isRotating = false;
		isIdle = true;
		speed = 2.0f;
		GetComponent<Image> ().rectTransform.sizeDelta = Vector2.zero;
	}
	
	public void AddImpulse(){
		oldSpeed = speed;
		speed *= 5;
		StartCoroutine (Decelerate ());
	}

	public void Stop(){
		StopAllCoroutines ();
		speed = 2.0f;
	}
	
	IEnumerator StartAnimating(){
		isIdle = true;
		float newAngle = Random.Range(400,320);
		while (isIdle) {
			float currentAngle = transform.rotation.eulerAngles.z < 320 ? transform.rotation.eulerAngles.z + 360 : transform.rotation.eulerAngles.z;
			if(!isRotating){
				if(Mathf.Abs(currentAngle - newAngle) < 10.0f){
					float tempAngle = Random.Range(400,320);
					if(Mathf.Abs(tempAngle - newAngle) > 10){
						newAngle = tempAngle;
					}
					
				}
			}
			else {
				newAngle = transform.rotation.eulerAngles.z + speed;
				if(newAngle + speed >= 360){
					newAngle -= 360;
				}
			}
			
			//Debug.Log(transform.rotation.eulerAngles + " | " + newAngle);
			float spd = speed * Time.deltaTime;
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis(newAngle, Vector3.forward), spd);
			yield return new WaitForSeconds(Time.deltaTime);
		}
	}
	
	IEnumerator Decelerate(){
		yield return new WaitForSeconds(0.5f);
		while (speed > oldSpeed) {
			speed -= speed/30;
			if(speed < oldSpeed){
				speed = oldSpeed;
			}
			yield return new WaitForSeconds(Time.deltaTime);
		}
	}
}
