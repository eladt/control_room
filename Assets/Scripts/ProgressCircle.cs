﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProgressCircle : MonoBehaviour {
	float lossSpeed = 0.30f;//% lost per second
	float step = 0.15f;//% added when tapping
	float nextCheck;
	Image img;
	float progress;
	float initialLossSpeed;
	float initialStep;

	void Awake(){
		initialLossSpeed = lossSpeed;
		initialStep = step;
	}

	void Start () {
		img = GetComponent<Image> ();
	}
	
	void Update () {
		if (img.fillAmount > 0 && img.fillAmount < 1 && nextCheck < Time.time) {
			progress -= lossSpeed/10;
			nextCheck = Time.time + 0.1f;
		}
		img.fillAmount = Mathf.Lerp(img.fillAmount, progress, 10.0f * Time.deltaTime);
	}

	public void increaseStep (){
		progress += step;
	}

	public float GetProgress(){
		return progress;
	}

	public void Hide(){
		SetProgress (0);
		lossSpeed = initialLossSpeed;
		step = initialStep;
	}

	public void SetProgress(float p){
		if (!img) {
			img = GetComponent<Image> ();
		}
		img.fillAmount = p;
		progress = p;
	}

	public void SetLevel(int level){
		step = initialStep - level * (step / 4);
		//lossSpeed = initialLossSpeed - level * (initialLossSpeed / 4);
	}
}
