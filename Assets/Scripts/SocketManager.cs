﻿using UnityEngine;
using System.Collections;
using WebSocketSharp;
using UnityEngine.UI;
using SimpleJSON;

public interface ISocketDelegate { 
	void socketConnected();
	void socketError ();
	void socketClosed ();
	void socketVoteOpened (int duration);
	void socketVoteClosed ();
	void socketGameFinished();
}

public class SocketManager : MonoBehaviour {
	WebSocket ws;
	ISocketDelegate deleg;

	void OpenWebSocket (string url) {
		Debug.Log (url);
		ws = new WebSocket (url);
		ws.OnOpen += (sender, e) => {
			//Debug.Log ("Connected: " + e.ToString());
			if(deleg != null){
				deleg.socketConnected();
			}
			DoOnMainThread.ExecuteOnMainThread.Enqueue(() => {StartCoroutine(sendPing());});

		};
		ws.OnMessage += (sender, e) =>{
			string msg = e.Data;
			var json = JSON.Parse(msg);
			string dataType = json["datatype"];
			if(dataType.Equals("CMD_TYPE_GET_PERFIL")){
				sendProfile();
			}
			if(dataType.Equals("CMD_TYPE_START_VOTING")){
				if(deleg != null){
					int duration = json["data"]["time"].AsInt;
					deleg.socketVoteOpened(duration);
				}
			}
			if(dataType.Equals("CMD_TYPE_STOP_VOTING")){
				if(deleg != null){
					deleg.socketVoteClosed();
				}
			}
			if(dataType.Equals("CMD_TYPE_DISCONNECT")){
				if(deleg != null){
					deleg.socketGameFinished();
					deleg = null;
					ws.Close();
					ws = null;
				}
			}
			Debug.Log ("Host says: " + e.Data.ToString());
		};
		ws.OnError += (sender, e) => {
			Debug.Log ("Error: " + e.Message);
			if(deleg != null){
				deleg.socketError();
			}
		};
		ws.OnClose += (sender, e) => {
			Debug.Log ("Disconnected: " + e.ToString());
		};
		ws.Connect();
	}

	public void btnPressed(){
		if (ws.IsAlive) {

		}
		else {
			Debug.Log("Connecting");
			OpenWebSocket("127.0.0.1:6001");
		}
	}

	public void Connect(ISocketDelegate d, string url){
		deleg = d;
		OpenWebSocket (url);
	}

	public void sendVote(int v){
		var json = new JSONClass();
		json["datatype"]  = "CMD_TYPE_VOTE";
		json ["error"] = "";
		json ["data"] ["index"] = v + "";
		ws.Send (json.ToString ());
		Debug.Log ("SENDING: "+json.ToString ());
	}

	void sendProfile (){
		var json = new JSONClass();
		json["datatype"]  = "CMD_TYPE_SET_PERFIL";
		json ["data"] ["name"] = "Richard II";
		//json ["data"] ["photo"] = "https://stallman.org/rms-icon.jpg";
		json ["data"] ["photo"] = "";
		ws.Send (json.ToString ());
		Debug.Log ("SENDING: "+json.ToString ());
	}

	IEnumerator sendPing(){
		while(ws != null){
			var json = new JSONClass();
			json["datatype"]  = "CMD_TYPE_PING";
			ws.Send (json.ToString ());
			Debug.Log ("SENDING: "+json.ToString ());
			yield return new WaitForSeconds(5);
		}

	}
}
