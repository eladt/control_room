﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using CielaSpike.Unity.Barcode;
using System.Threading;

public class QRHandler : MonoBehaviour {
	public InputField txtResult;
	public Button btnClose;
	public GameObject plane;

	GameController gc;
	WebCamTexture cameraTexture;
	Material cameraMat;
	WebCamDecoder decoder;
	bool decoding;

	void Start(){
		gc = Camera.main.GetComponent<GameController> ();
		Deactivate ();
	}

	public void Activate () {
		gameObject.SetActive (true);
		btnClose.gameObject.SetActive (false);
	}

	public void Deactivate () {
		gameObject.SetActive (false);
		StopCamDecoder();
	}

	public void InitializeDecoder(){
		cameraMat = plane.GetComponent<MeshRenderer>().material;

		// get a reference to web cam decoder component;
		decoder = GetComponent<WebCamDecoder>();
		decoder.Options.TryHarder = true;
	}

	public void ScanCode (){
		if(gc.screen == GameController.ScreenTypes.ScreenQR && !decoding){
//			Debug.Log ("Not yet!");
//			gc.LoginToServer ("192.168.1.221:6001");
			LaunchCamDecoder ();
		}
	}

	public void URLFieldDoneEditing (){
		if(gc.screen == GameController.ScreenTypes.ScreenQR){
			Debug.Log ("Done editing");
			gc.LoginToServer (txtResult.text);
		}
	}

	public void OfflineModePressed(){
		gc.StartOfflineMode();
	}

	void Update(){
		if(!decoding){
			return;
		}
		// get decode result;
		DecodeResult result = decoder.Result;
		if (result.Success && txtResult.text != result.Text)
		{
			txtResult.text = result.Text;
			decoding = false;
			//gc.LoginToServer (result.Text);
			Debug.Log(string.Format(
				"Decoded: [{0}]{1}", result.BarcodeType, result.Text));
			CloseCamDecoderPressed();
		}
	}

	void LaunchCamDecoder(){
		decoding = true;
		gc.PauseQRScreen ();
		btnClose.gameObject.SetActive (true);

		if(plane == null){
			InitializeDecoder();
		}

		//plane.transform.localScale = new Vector3(20,1,15.5f);
		plane.transform.localScale = new Vector3(114,1,64);

		//Start recording
		var deviceName = WebCamTexture.devices[0].name;
		if(WebCamTexture.devices.Length > 1){
			deviceName = WebCamTexture.devices[1].name;
		}
		cameraTexture = new WebCamTexture(deviceName, 640, 360, 30);
		cameraTexture.Play();
		cameraMat.mainTexture = cameraTexture;
		
		plane.transform.rotation = plane.transform.rotation * 
			Quaternion.AngleAxis(cameraTexture.videoRotationAngle, Vector3.up);

		StartCoroutine(decoder.StartDecoding(cameraTexture));
	}

	void StopCamDecoder(){
		btnClose.gameObject.SetActive (false);
		decoding = false;
		if(camera != null){
			cameraTexture.Stop ();
		}
		if(decoder != null){
			decoder.StopDecoding();
		}
	}

	public void CloseCamDecoderPressed(){
		plane.transform.localScale = new Vector3(0,0,0);
		StopCamDecoder ();
		gc.ResumeQRScreen ();
	}
}
