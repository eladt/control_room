﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BPMCircle : MonoBehaviour {
	public Text txt;
	Image img;
	int bpm;
	int actualBpm;
	float nextUpdateCheck;
	
	void Start () {
		img = GetComponent<Image> ();
	}
	
	void Update () {
		if(bpm > 0){
			bpm--;
		}
		actualBpm = (int)Mathf.Lerp(actualBpm, bpm, 1.0f * Time.deltaTime);
		img.fillAmount = actualBpm / 700.0f;
		txt.text = actualBpm + "";
	}
	
	public int GetBPM(){
		return bpm;
	}

	public void SetBPM(int b){
		if (b < 0) {
			b = 0;
		}
		bpm = b;
		//txt.text = bpm + "";
	}

	public void Reset() {
		bpm = 0;
		actualBpm = 0;
	}

}
